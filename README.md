# Aquarius Connector
"Aquarius provides an off-chain database store for metadata about data assets. When running with our Docker images, it is exposed under: http://0.0.0.0:5000"

Documentation: https://docs.oceanprotocol.com/references/aquarius/

Specification: https://github.com/Enspire-Tech/openapi-connector-artifacts/blob/master/quickbase/custom-specification-aquarius.yaml

## Prerequisites

+ A running Aquarius instance

## Supported Operations

**2 out of 11 endpoints are failing.**

The following operations are **not** supported at this time:
* /api/v1/aquarius/assets/ddo/encrypt
* /api/v1/aquarius/assets/ddo/encryptashex



## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

